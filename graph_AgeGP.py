
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

import age_GP

import statsmodels.api as sm

"""
This script is meant to create the graph for showing the relationship between a players Age and how many games they played.

Author: Damian Chavira

"""

#Import data files

"""
    This function is where the data is plotted and shown.
    Params -- cleanData: cleaned up dataframe to be graphed.
    Returns -- None
"""
def make_graph(cleanData):
    
    plt.title("Age vs Games Played",fontsize=20)
    plt.ylabel("Games Played ")
    plt.xlabel("Age")
    plt.scatter(x=cleanData.index,y=cleanData['G'])
    x=cleanData.index.values
    
    X=sm.add_constant(x)
    model = sm.OLS(cleanData,X)
    line=model.fit()
    y=line.params['x1']*x+line.params['const']
    plt.plot(x,y,linewidth=1,color='r')
    plt.show()
    
"""
    This function is where the dataframe is recieved and cleaned up

    Params -- None
    Returns -- cleanData: A dataframe of positions and their ratings.
"""
def createData():
    cleanData=age_GP.get_data("Age",'G')
    
    cleanData.set_index('Age',inplace=True)
    
    return cleanData

    
def main():
    cleanData=createData()
    
    make_graph(cleanData)
    