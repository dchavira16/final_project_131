import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

import MINUTES_PLAYED_VS_3_POINT_ATTEMPT_RATE

import statsmodels.api as sm

"""
This script is meant to create the graph for showing different positions efficiency ratings and see which 
one is most efficient among all.

Author: Kyle McLemore

"""

#Import data files

"""
    This function is where the bar graph is plotted and shown.
    Params -- cleanData: cleaned up dataframe to be graphed.
    Returns -- None
"""
def make_graph(cleanData):
    
    plt.title("Minutes Played VS 3PT Attempt Rate",fontsize=20)
    plt.ylabel("3PT Attempt Rate")
    plt.xlabel("Minutes Played")
    plt.scatter(x=cleanData.index,y=cleanData['3_Point_Attempt_Rate'])
    x=cleanData.index.values
    
    X=sm.add_constant(x)
    model = sm.OLS(cleanData,X)
    line=model.fit()
    y=line.params['x1']*x+line.params['const']
    plt.plot(x,y,linewidth=1,color='r')
    plt.show()
    
"""
    This function is where the dataframe for different positions efficiency ratings are to be retrieved and cleaned up.

    Params -- None
    Returns -- cleanData: A dataframe of positions and their ratings.
"""
def createData():
    cleanData=MINUTES_PLAYED_VS_3_POINT_ATTEMPT_RATE.get_data('MP','3PAr')
    
    
    return cleanData

    
def main():
    cleanData=createData()
    make_graph(cleanData)
    