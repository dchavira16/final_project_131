import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

import POSITION_VS_AGE

import statsmodels.api as sm

"""
This script is meant to create the graph for showing the relationship between Age and Free throw rate.

Free Throw Rate Formula= (Total Free Throws Attempted) / (Total Field Goal Attempted)]

Author: Kyle McLemore

"""

#Import data files

"""
    This function is where the bar graph is plotted and shown.
    Params -- cleanData: cleaned up dataframe to be graphed.
    Returns -- None
"""
def make_graph(cleanData):
    
    plt.gcf().subplots_adjust(bottom=.2)
    plt.title("Average Position Age",fontsize=20)
    plt.xticks(rotation=30, horizontalalignment="center")
    for i in  ((cleanData.index.values)):
        plt.bar(i,cleanData['Age'][i])
    plt.ylabel("Age")
    plt.xlabel("Position")
    plt.show()
    
"""
    This function is where the dataframe for different positions efficiency ratings are to be retrieved and cleaned up.

    Params -- None
    Returns -- cleanData: A dataframe of positions and their ratings.
"""
def createData():
    cleanData=POSITION_VS_AGE.get_data('Pos','Age')
    
    
    return cleanData

    
def main():
    cleanData=createData()
    make_graph(cleanData)
    