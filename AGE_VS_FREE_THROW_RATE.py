import pandas as pd
import matplotlib.pyplot as plt


#Free Throw Rate Formula= (Total Free Throws Attempted) / (Total Field Goal Attempted)]
def get_data(*val):
    ages = []
    avg = []
    FTr = []
    data = pd.read_csv('nba_2020.csv', usecols=val)
    data = data.sort_values(by=[val[0]])
    data = data.reset_index(drop=True)
    data = data.fillna(0)

    for x in range(len(data['Age'].values)):
        if data['Age'].values[x] not in ages:
            ages.append(data['Age'].values[x])

    for x in range(len(ages)):
        for i in range(len(data['FTr'])):
            if data['Age'][i] == ages[x]:
                avg.append(data['FTr'][i])
        FTr.append(round(sum(avg)/len(avg),2))
        avg = []

    data = pd.DataFrame(data=FTr,columns=['Free_Throw_Rate'],index=ages)
    return(data)


#get_data('Age','FTr')