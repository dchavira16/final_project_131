import pandas as pd
import matplotlib.pyplot as plt

#MINUTES PLAYED VS SHOOTING PERCENTAGE
def get_data(*val):
    data = pd.read_csv('nba_2020.csv', usecols=val)
    data = data.sort_values(by=[val[0]])
    data = data.reset_index(drop=True)
    return(data)
