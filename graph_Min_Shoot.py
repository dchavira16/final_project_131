import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import min_shoot

import statsmodels.api as sm

"""
This script is meant to create the graph for showing the relationship between players shooting percentage and
the amount of minutes players play.
The prediction was that as players play more minutes, their shooting percentage increases.
This graph proves that.

Author: Damian Chavira

"""


#Import data files

"""
    This function is where the scatter plot is created along with the regression line.
    Params -- cleanData: cleaned up dataframe to be graphed.
    Returns -- None
"""
def make_graph(cleanData):
    plt.title("Minutes Played VS Shooting Percentage",fontsize=20)
    plt.ylabel("Shooting Percentage (%) ")
    plt.xlabel("Minutes Played (Minutes)")
    plt.scatter(x=cleanData.index,y=cleanData['shooting'])
    x=cleanData.index.values
    
    X=sm.add_constant(x)
    model = sm.OLS(cleanData,X)
    line=model.fit()
    y=line.params['x1']*x+line.params['const']
    plt.plot(x,y,linewidth=1,color='r')
    plt.show()
"""
    This function is where the different data (minutes played and shooting percentage) for all players
    gets combined into one dataframe. Also removed any NaNs values.

    Params -- None
    Returns -- cleanData: A combined dataframe of (minutes vs shooting percentage) that has removed
                any NaNs. Also makes the minutes the index values since the minutes and shooting are for the same player for each index,
                so combining them saves space and allows easier plotting.
"""
def createData():
    m_Data=min_shoot.get_data('MP')
    s_Data=min_shoot.get_data('TS%')
    minutes=[]
    shooting=[]
    for val in m_Data.values:
        minutes.append((val[0]))
    for val in s_Data.values:
        shooting.append((val[0]))
    cleanData=pd.DataFrame({'minutes': minutes, 'shooting': shooting}, columns=['minutes', 'shooting'],index=minutes)
    cleanData.set_index('minutes',inplace=True)
    cleanData=cleanData.dropna()
    return cleanData

    
def main():
    cleanData=createData()
    make_graph(cleanData)
