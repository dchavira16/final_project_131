import pandas as pd
import matplotlib.pyplot as plt


#POSITION VS EFFICIENCY RATING
def get_data(*val):
    positions = ['Center','Power Forward','Small Forward','Shooting Guard','Point Guard']
    position_abreviations = ['C','PF','SF','SG','PG']
    avg = []
    age = []
    data = pd.read_csv('nba_2020.csv', usecols=val)
    data = data.sort_values(by=[val[0]])
    data = data.reset_index(drop=True)

    for x in range(len(position_abreviations)):
        for i in range(len(data['Pos'])):
            if data['Pos'][i] == position_abreviations[x]:
                avg.append(data['Age'][i])
        age.append(round(sum(avg)/len(avg),2))
        avg = []

    data = pd.DataFrame(data=age,columns=['Age'],index=positions)
    return(data)


#get_data('Pos','Age')
