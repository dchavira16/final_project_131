import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

import AGE_VS_FREE_THROW_RATE

import statsmodels.api as sm

"""
This script is meant to create the graph for showing the relationship between Age and Free throw rate.

Free Throw Rate Formula= (Total Free Throws Attempted) / (Total Field Goal Attempted)]

Author: Kyle McLemore

"""

#Import data files

"""
    This function is where the bar graph is plotted and shown.
    Params -- cleanData: cleaned up dataframe to be graphed.
    Returns -- None
"""
def make_graph(cleanData):
    
    plt.gcf().subplots_adjust(bottom=.2)
    plt.title("Age vs Free Throw Rate",fontsize=20)
    plt.xticks(rotation=30, horizontalalignment="center")
    for i in  ((cleanData.index.values)):
        plt.bar(i,cleanData['Free_Throw_Rate'][i])
    plt.ylabel("Free Throw Rate")
    plt.xlabel("Age")
    plt.show()
    
"""
    This function is where the dataframe for different positions efficiency ratings are to be retrieved and cleaned up.

    Params -- None
    Returns -- cleanData: A dataframe of positions and their ratings.
"""
def createData():
    cleanData=AGE_VS_FREE_THROW_RATE.get_data('Age','FTr')
    
    
    return cleanData

    
def main():
    cleanData=createData()
    make_graph(cleanData)
    