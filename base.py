import graph_Min_Shoot
import graph_PosEff
import graph_AgeGP
import age_FT_graph1
import mp_3p_graph1
import pos_age_graph1
#Import data files
"""
This file is meant to link up all data and graphs together in one for instant demonstration.

"""
#Damian's Graphs
graph_Min_Shoot.main()
graph_PosEff.main()
graph_AgeGP.main()

#Kyle's Graphs
age_FT_graph1.main()
mp_3p_graph1.main()
pos_age_graph1.main()