import pandas as pd
import matplotlib.pyplot as plt

#AGE VS GAMES PLAYED
def get_data(*val):
    data = pd.read_csv('nba_2020.csv', usecols=val)
    data = data.sort_values(by=[val[0]])
    data = data.reset_index(drop=True)
    data = data.fillna(0)
    data = pd.DataFrame(data=data['3PAr'].values,columns=['3_Point_Attempt_Rate'],index=data['MP'].values)
    return(data)

#get_data('MP','3PAr')