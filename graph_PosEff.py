
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

import pos_eff

import statsmodels.api as sm

"""
This script is meant to create the graph for showing different positions efficiency ratings and see which 
one is most efficient among all.

Author: Damian Chavira

"""

#Import data files

"""
    This function is where the bar graph is plotted and shown.
    Params -- cleanData: cleaned up dataframe to be graphed.
    Returns -- None
"""
def make_graph(cleanData):
    
    plt.gcf().subplots_adjust(bottom=.2)
    plt.title("Most Efficient Position",fontsize=20)
    plt.xticks(rotation=30, horizontalalignment="center")
    for i in range (0,len(cleanData.index.values)):
        plt.bar(i,cleanData['Position'][i])
    plt.xticks(np.arange(5),cleanData.index.values)
    plt.ylabel("Efficiency Rating")
    plt.xlabel("Positions")
    plt.show()
    
"""
    This function is where the dataframe for different positions efficiency ratings are to be retrieved and cleaned up.

    Params -- None
    Returns -- cleanData: A dataframe of positions and their ratings.
"""
def createData():
    cleanData=pos_eff.get_data('Pos','PER')
    
    return cleanData

    
def main():
    cleanData=createData()
    make_graph(cleanData)
    